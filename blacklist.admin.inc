<?php

/**
 * @file
 * Module configuration page callbacks.
 */

/**
 * The configuration form.
 */
function blacklist_admin_form() {
  $form = array();
  $form['ip_blocked'] = array(
    '#type' => 'textarea',
    '#title' => t('Blocked IP message'),
    '#description' => t('This message will be shown when a user from a blocked IP or IP range attempts signing in, or registering.'),
    '#default_value' => variable_get('blacklist_ip_blocked', _default_ip_blocked()),
  );
  $form['name_blocked'] = array(
    '#type' => 'textarea',
    '#title' => t('Blocked name message'),
    '#description' => t('This message will be shown when a user attempts to sign in with a blocked username.'),
    '#default_value' => variable_get('blacklist_name_blocked', _default_name_blocked()),
  );
  $form['mail_blocked'] = array(
    '#type' => 'textarea',
    '#title' => t('Blocked mail message'),
    '#description' => t('This message will be shown when a user attempts to sign in with a blocked mail address.'),
    '#default_value' => variable_get('blacklist_mail_blocked', _default_mail_blocked()),
  );
  return system_settings_form($form);
}
